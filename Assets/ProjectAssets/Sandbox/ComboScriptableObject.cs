using UnityEngine;

namespace CG.Group3.Project.Sandbox
{
    public abstract class ComboScriptableObject : ScriptableObject
    {
        // TODO actually carry out the combo
        public abstract void DoAction(FederatedControllerBase callingFederatedController, object data);
    }
}
