﻿using Fusion;

namespace CG.Group3.Project.Sandbox
{
    public class MovementFederatedController : FederatedControllerBase
    {
        public override char Tag => 'm';
        public override int Index => 1;

        public override bool ProduceInput(ref ActionData data)
        {
            base.ProduceInput(ref data);

            var jumpPressed = Controls.Robot.Jump.WasPressedThisFrame();
            var movedLeft = Controls.Robot.Move.ReadValue<float>() < 0;
            var movedRight = Controls.Robot.Move.ReadValue<float>() > 0;

            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (!jumpPressed && !movedLeft && !movedRight) return false;
            
            if (jumpPressed)
            {
                data.Buttons.SetDown(ActionButton.Jump);
            }

            if (movedLeft)
            {
                data.Buttons.SetDown(ActionButton.MoveLeft);
            }

            if (movedRight)
            {
                data.Buttons.SetDown(ActionButton.MoveRight);
            }

            return true;
        }

        protected override bool ConsumeInput(ActionData data, out NetworkButtons buttonsPressedThisFrame)
        {
            if (!base.ConsumeInput(data, out buttonsPressedThisFrame)) return false;

            return Robot.Move(data) | Robot.Jump(buttonsPressedThisFrame);
        }
    }
}