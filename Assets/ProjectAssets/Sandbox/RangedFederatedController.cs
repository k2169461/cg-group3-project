﻿using Fusion;

namespace CG.Group3.Project.Sandbox
{
    public class RangedFederatedController : FederatedControllerBase
    {
        public override char Tag => 'r';
        public override int Index => 4;
        
        public override bool ProduceInput(ref ActionData data)
        {
            return base.ProduceInput(ref data);
        }

        protected override bool ConsumeInput(ActionData data, out NetworkButtons buttonsPressedThisFrame)
        {
            return base.ConsumeInput(data, out buttonsPressedThisFrame);
        }
    }
}