using Fusion;

namespace CG.Group3.Project.Sandbox
{
    public enum ActionButton
    {
        DebugAction,
        MoveLeft,
        MoveRight,
        Jump,
        ComboStart,
        ComboEngageA,
        ComboEngageB,
        ComboEngageX,
        ComboEngageY,
        // For clearer debug messages. Names are not used in code yet.
        ComboEngageAMovement = ComboEngageA + 1 * 4,
        ComboEngageBMovement = ComboEngageB + 1 * 4,
        ComboEngageXMovement = ComboEngageX + 1 * 4,
        ComboEngageYMovement = ComboEngageY + 1 * 4,
        ComboEngageAMelee = ComboEngageA + 2 * 4,
        ComboEngageBMelee = ComboEngageB + 2 * 4,
        ComboEngageXMelee = ComboEngageX + 2 * 4,
        ComboEngageYMelee = ComboEngageY + 2 * 4,
        ComboEngageAShield = ComboEngageA + 3 * 4,
        ComboEngageBShield = ComboEngageB + 3 * 4,
        ComboEngageXShield = ComboEngageX + 3 * 4,
        ComboEngageYShield = ComboEngageY + 3 * 4,
        ComboEngageARanged = ComboEngageA + 4 * 4,
        ComboEngageBRanged = ComboEngageB + 4 * 4,
        ComboEngageXRanged = ComboEngageX + 4 * 4,
        ComboEngageYRanged = ComboEngageY + 4 * 4,
        // The maximum shift allowed by NetworkButtons
        Max = sizeof(int) * 8 - 1
    }
    
    public static class ActionButtonExtensions
    {
        /// Returns an offset value of the given combo button base value using the supplied federated controller's Index property.
        public static ActionButton ForFc(this ActionButton comboButtonBase, in FederatedControllerBase fc)
        {
            Assert.Check(comboButtonBase is >= ActionButton.ComboEngageA and <= ActionButton.ComboEngageY,
                "Invalid combo base button. Should be ComboEngage[ABXY]Base.");
            return comboButtonBase + fc.Index * 4;
        }
    }

    public struct ActionData : INetworkInput
    {
        public NetworkButtons Buttons;
    }
}