﻿using Fusion;

namespace CG.Group3.Project.Sandbox
{
    public class ShieldFederatedController : FederatedControllerBase
    {
        public override char Tag => 's';
        public override int Index => 3;
        
        public override bool ProduceInput(ref ActionData data)
        {
            return base.ProduceInput(ref data);
        }

        protected override bool ConsumeInput(ActionData data, out NetworkButtons buttonsPressedThisFrame)
        {
            return base.ConsumeInput(data, out buttonsPressedThisFrame);
        }
    }
}