﻿using UnityEngine;

namespace CG.Group3.Project.Sandbox
{
    [CreateAssetMenu(menuName = "Create DebugComboScriptableObject1", fileName = "DebugComboScriptableObject1",
        order = 0)]
    public class DebugComboScriptableObject1 : ComboScriptableObject
    {
        public override void DoAction(FederatedControllerBase callingFederatedController, object data)
        {
            Debug.LogError($"Action performed by {callingFederatedController.Object.InputAuthority}.");
        }
    }
}