﻿using Fusion;
using TMPro;
using UnityEngine;

namespace CG.Group3.Project.Sandbox
{
    public class DebugFederatedController : FederatedControllerBase
    {
        [Networked] private TickTimer MessageClearDelay { get; set; }
        private TextMeshProUGUI messagesTMPro;

        public override char Tag => 'x';
        public override int Index => 0;

        public override bool ProduceInput(ref ActionData data)
        {
            base.ProduceInput(ref data);
            
            if (!Controls.Robot.DebugAction.WasPressedThisFrame()) return false;
            
            data.Buttons.SetDown(ActionButton.DebugAction);
            return true;
        }

        public override void FixedUpdateNetwork()
        {
            base.FixedUpdateNetwork();

            if (MessageClearDelay.ExpiredOrNotRunning(Runner) && messagesTMPro)
            {
                messagesTMPro.text = "";
            }
        }

        protected override bool ConsumeInput(ActionData data, out NetworkButtons buttonsPressedThisFrame)
        {
            if (!base.ConsumeInput(data, out buttonsPressedThisFrame)) return false;
            
            return Robot.DebugAction(data, () =>
            {
                MessageClearDelay = TickTimer.CreateFromSeconds(Runner, 5f);
                RPCSendMessage("Debug Action triggered!~");
            });
        }

        [Rpc(RpcSources.All, RpcTargets.All)]
        // ReSharper disable once UnusedParameter.Local
        private void RPCSendMessage(string message, RpcInfo info = default)
        {
            if (!messagesTMPro)
            {
                messagesTMPro = GameObject.FindWithTag("MessagesTMPro").GetComponent<TextMeshProUGUI>();
            }

            // Invocation occurs on the host from client input
            messagesTMPro.text += $"{Object.InputAuthority} said: {message}\n";
        }
    }
}