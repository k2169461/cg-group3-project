using System;
using System.Collections.Generic;
using System.Linq;
using CG.Group3.Project.Utility;
using Fusion;
using Fusion.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;
using UnityEngine.Serialization;
using Random = UnityEngine.Random;

namespace CG.Group3.Project.Sandbox
{
    public class GameManager : MonoBehaviour, INetworkRunnerCallbacks
    {
        [SerializeField] private Robot robotPrefab;
        [FormerlySerializedAs("controlProxyPrefabRefs")]
        [SerializeField] private NetworkPrefabRef[] federatedControllerPrefabRefs;
        private readonly Dictionary<PlayerRef, FederatedControllerBase> playerToFcMap = new();
        private ActionData actionData;
        private readonly HashSet<NetworkPrefabRef> fcPrefabRefsAvailable = new();
        private readonly Dictionary<NetworkPrefabRef, FederatedControllerBase> fcPrefabsSpawned = new();

        public Robot Robot { get; private set; }

        // For use with Unity Events
        [EnumAction(typeof(GameMode))]
        public void StartGame(int gameMode) => StartGame((GameMode)gameMode);

        private async void StartGame(GameMode mode)
        {
            var runner = FindObjectOfType<NetworkRunner>();
            if (!runner) return;

            runner.ProvideInput = true;
            runner.AddCallbacks(this);

            // Start or join (depends on game mode) a session with a specific name
            await runner.StartGame(new StartGameArgs
            {
                GameMode = mode,
                SessionName = "TestRoom",
                PlayerCount = 4,
                Scene = SceneManager.GetActiveScene().buildIndex,
                SceneManager = gameObject.AddComponent<NetworkSceneManagerDefault>()
            });
        }

        void INetworkRunnerCallbacks.OnPlayerJoined(NetworkRunner runner, PlayerRef player)
        {
            // One-time setup
            if (!Robot)
            {
                // The robot belongs to the host-client/state authority
                Robot = runner.Spawn(robotPrefab, Vector3.zero, Quaternion.identity);
                if (Robot)
                {
                    Robot.Object.AssignInputAuthority(Robot.Object.StateAuthority);
                }

                foreach (var fcPrefabRef in federatedControllerPrefabRefs)
                {
                    fcPrefabRefsAvailable.Add(fcPrefabRef);
                }
            }

            // Find a random controller to give to the new player
            // Controllers may not all be possessed
            var randomFcPrefabRef =
                fcPrefabRefsAvailable.ElementAtOrDefault(Random.Range(0, fcPrefabRefsAvailable.Count));
            var fcNetworkObject = runner.Spawn(randomFcPrefabRef, Vector3.zero, Quaternion.identity, player);

            // Only works on the host, so this is null for clients.
            if (!fcNetworkObject) return;
            var fc = fcNetworkObject.GetComponent<FederatedControllerBase>();
            fcPrefabRefsAvailable.Remove(randomFcPrefabRef);
            fcPrefabsSpawned.Add(randomFcPrefabRef, fc);
        }

        void INetworkRunnerCallbacks.OnPlayerLeft(NetworkRunner runner, PlayerRef player)
        {
            // No player, no object.
            if (!runner.TryGetPlayerObject(player, out _)) return;

            // Return the controller to the set of available controllers and remove the player
            if (!playerToFcMap.TryGetValue(player, out var fc)) return;
            runner.Despawn(fc.Object);
            var prefabRef = fcPrefabsSpawned.First(kv => kv.Value == fc).Key;
            fcPrefabsSpawned.Remove(prefabRef);
            fcPrefabRefsAvailable.Add(prefabRef);
        }

        private void Update()
        {
            // Care should be taken that the controllers don't overwrite each other each time
            foreach (var fc in playerToFcMap.Values.Where(fc => fc && fc.HasInputAuthority))
            {
                if (fc.ProduceInput(ref actionData))
                {
                    Debug.Log($"Input produced by {fc.Object.InputAuthority} from type {fc.GetType()}");
                }
            }
        }

        void INetworkRunnerCallbacks.OnInput(NetworkRunner runner, NetworkInput input)
        {
            input.Set(actionData);
            actionData = default;
        }

        void INetworkRunnerCallbacks.OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input)
        {
        }

        void INetworkRunnerCallbacks.OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason)
        {
            SceneManager.LoadScene(SceneManager.GetActiveScene().buildIndex);
        }

        // ReSharper disable once Unity.IncorrectMethodSignature
        void INetworkRunnerCallbacks.OnConnectedToServer(NetworkRunner runner)
        {
        }

        void INetworkRunnerCallbacks.OnDisconnectedFromServer(NetworkRunner runner)
        {
        }

        void INetworkRunnerCallbacks.OnConnectRequest(NetworkRunner runner,
            NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
        {
        }

        void INetworkRunnerCallbacks.OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress,
            NetConnectFailedReason reason)
        {
        }

        void INetworkRunnerCallbacks.OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message)
        {
        }

        void INetworkRunnerCallbacks.OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
        {
        }

        void INetworkRunnerCallbacks.OnCustomAuthenticationResponse(NetworkRunner runner,
            Dictionary<string, object> data)
        {
        }

        void INetworkRunnerCallbacks.OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken)
        {
        }

        void INetworkRunnerCallbacks.OnReliableDataReceived(NetworkRunner runner, PlayerRef player,
            ArraySegment<byte> data)
        {
        }

        void INetworkRunnerCallbacks.OnSceneLoadDone(NetworkRunner runner)
        {
        }

        void INetworkRunnerCallbacks.OnSceneLoadStart(NetworkRunner runner)
        {
        }

        public void AddPlayerToFcMapping(in PlayerRef playerRef, in FederatedControllerBase federatedController) =>
            playerToFcMap.Add(playerRef, federatedController);

        public void RemovePlayerToFcMapping(in PlayerRef playerRef) =>
            playerToFcMap.Remove(playerRef);
    }
}