﻿namespace CG.Group3.Project.Sandbox
{
    public enum ComboSmState
    {
        Neutral,
        Initializing,
        Engaged,
        CoolingDown,
    }
}