using System;
using Fusion;
using UnityEngine;

namespace CG.Group3.Project.Sandbox
{
    public class Robot : NetworkBehaviour
    {
        [SerializeField, Min(0f)] private float moveMaxSpeed = 10f;
        [SerializeField, Min(0f)] private float jumpImpulseY = 10f;
        [Networked] private int FacingDirection { get; set; } = 1;
        private Rigidbody2D _rb2d;
        private Transform _xfm;

        private void Awake()
        {
            _xfm = transform;
            _rb2d = GetComponent<Rigidbody2D>();
        }

        public override void FixedUpdateNetwork()
        {
            FlipRobot();

            void FlipRobot()
            {
                var newScale = _xfm.localScale;
                newScale.x = FacingDirection * Mathf.Abs(newScale.x);
                _xfm.localScale = newScale;
            }
        }

        public bool Move(ActionData data)
        {
            var movingLeft = data.Buttons.IsSet(ActionButton.MoveLeft);
            var movingRight = data.Buttons.IsSet(ActionButton.MoveRight);
            if (!movingLeft && !movingRight) return false;

            // The facing direction must be updated based on the input
            if ((movingLeft && FacingDirection > 0) || (movingRight && FacingDirection < 0))
            {
                FacingDirection *= -1;
            }

            // This velocity is only applied while input is received, making the robot slow down as if it were skidding on ice otherwise
            // Can be altered using physics materials
            var newV = _rb2d.velocity;
            newV.x = FacingDirection * moveMaxSpeed;
            _rb2d.velocity = newV;

            return true;
        }

        public bool Jump(NetworkButtons pressedButtons)
        {
            if (!pressedButtons.IsSet(ActionButton.Jump)) return false;

            // Only allow jumping when not already flying upwards
            if (_rb2d.velocity.y <= 0)
            {
                _rb2d.AddForce(new Vector2(0f, jumpImpulseY), ForceMode2D.Impulse);
            }

            return true;
        }

        public static bool DebugAction(ActionData data, Action action)
        {
            if (!data.Buttons.IsSet(ActionButton.DebugAction)) return false;

            action?.Invoke();
            return true;
        }
    }
}