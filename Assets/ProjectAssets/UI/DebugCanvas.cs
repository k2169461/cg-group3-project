using Fusion;
using TMPro;
using UnityEngine;

namespace CG.Group3.Project.UI
{
    public class DebugCanvas : NetworkBehaviour
    {
        [SerializeField] private TextMeshProUGUI debugMessages;
        [SerializeField, Min(1f)] private float messageWipeDelaySeconds = 5f;
        [Networked] private TickTimer WipeTimer { get; set; }

        public override void FixedUpdateNetwork()
        {
            if (WipeTimer.ExpiredOrNotRunning(Runner))
            {
                debugMessages.text = "";
            }
        }

        [Rpc(RpcSources.All, RpcTargets.All)]
        public void RpcPrintTextToScreen(NetworkString<_16> message)
        {
            // Newest message appears at the top with a timestamp
            debugMessages.text = $"[{Runner.SimulationTime:.00}] {message}\n" + debugMessages.text;
            WipeTimer = TickTimer.CreateFromSeconds(Runner, messageWipeDelaySeconds);
        }
    }
}
