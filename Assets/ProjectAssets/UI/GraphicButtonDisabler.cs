using UnityEngine;

namespace CG.Group3.Project.UI
{
    [ExecuteAlways]
    public class GraphicButtonDisabler : MonoBehaviour
    {
        // Start is called before the first frame update
        private void Start() =>
            GetComponentInParent<GraphicButtonInteractionProvider>()
                .DimOutGraphicButton(GetComponent<RectTransform>());
    }
}