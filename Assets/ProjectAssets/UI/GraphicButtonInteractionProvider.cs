using System;
using System.Collections.Generic;
using System.Linq;
using Fusion;
using UnityEngine;
using UnityEngine.Serialization;
using UnityEngine.UI;

namespace CG.Group3.Project.UI
{
    public class GraphicButtonInteractionProvider : NetworkBehaviour
    {
        private const string ErrorMessage = "Invalid input.";

        [Serializable]
        public class DbRow
        {
            public RectTransform rectTransform;
            [FormerlySerializedAs("scale")]
            [Min(1.01f)] public float hoverScale = 1.1f;
            public NetworkPrefabRef fcPrefabRef;
        }

        [SerializeField, Range(0.01f, 0.99f)] private float dimAmount = 0.5f;
        // Get around the limitation of UnityEvents only supporting one parameter in-editor
        [FormerlySerializedAs("data")]
        [SerializeField] private List<DbRow> db;

        public IEnumerable<DbRow> Db => db;

        public void Scale(RectTransform rectTransform)
        {
            var x = db.FirstOrDefault(datum => datum.rectTransform == rectTransform);
            Assert.Check(x != null, ErrorMessage);
            var scale = x!.hoverScale;
            rectTransform.localScale = new Vector3(scale, scale, scale);
        }

        public void Unscale(RectTransform rectTransform)
        {
            Assert.Check(db.Any(datum => datum.rectTransform == rectTransform), ErrorMessage);
            rectTransform.localScale = Vector3.one;
        }

        [Rpc(RpcSources.StateAuthority, RpcTargets.All)]
        // ReSharper disable once UnusedParameter.Global
        public void RpcDimOutGraphicButton(NetworkPrefabRef fcPrefabRef, RpcInfo rpcInfo = default)
        {
            var dbRow = db.FirstOrDefault(dbRow => dbRow.fcPrefabRef == fcPrefabRef);
            Assert.Check(dbRow != null, ErrorMessage);
            
            DimOutGraphicButton(dbRow!.rectTransform, false);
        }

        public void DimOutGraphicButton(RectTransform rectTransform, bool assert = true)
        {
            if (assert)
            {
                Assert.Check(db.Any(datum => datum.rectTransform == rectTransform), ErrorMessage);
            }

            var button = rectTransform.GetComponent<Button>();

            if (!button.interactable) return;

            foreach (var imageDescendant in rectTransform.GetComponentsInChildren<Image>()
                         .Where(image => image != rectTransform.GetComponent<Image>()))
            {
                var newColor = imageDescendant.color;
                newColor.a = 1 - dimAmount;
                imageDescendant.color = newColor;
            }

            button.interactable = false;
        }

        [Rpc(RpcSources.StateAuthority, RpcTargets.All)]
        public void RpcActivateSelf(bool state)
        {
            gameObject.SetActive(state);
        }
    }
}