using CG.Group3.Project.UI;
using Fusion;
using UnityEngine.UI;

namespace CG.Group3.Project.Game
{
    public class GameManagerRpcForwarder : NetworkBehaviour
    {
        private GameManager gameManager;

        public void Init(in GameManager newGameManager, in GraphicButtonInteractionProvider gbip)
        {
            gameManager = newGameManager!;

            foreach (var row in gbip!.Db)
            {
                var button = row.rectTransform.GetComponent<Button>();
                button.onClick.AddListener(() => RpcAssignFcToPlayer(row.fcPrefabRef));
            }
        }

        [Rpc(RpcSources.All, RpcTargets.StateAuthority)]
        private void RpcAssignFcToPlayer(NetworkPrefabRef fcPrefabRef)
        {
            gameManager.AssignFcToPlayerImpl(fcPrefabRef, Runner.LocalPlayer);
        }
    }
}
