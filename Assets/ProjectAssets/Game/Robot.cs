using Fusion;
using UnityEngine;

namespace CG.Group3.Project.Game
{
    public class Robot : NetworkBehaviour
    {
        [SerializeField, Min(0f)] private float moveMaxSpeed = 10f;
        [SerializeField, Min(0f)] private float jumpImpulseY = 10f;
        [SerializeField] private Animator movementAnimator;
        [SerializeField] private Transform gunHandShoulder;
        [SerializeField, Range(0f, 45f)] private float maxAimAngle = 45f;
        [Networked, UnityNonSerialized] private int FacingDirection { get; set; } = 1;
        private Transform xfm;
        private Rigidbody2D rb2d;
        private static readonly int ShouldWalk = Animator.StringToHash("shouldWalk");

        private void Awake()
        {
            xfm = transform;
            rb2d = GetComponent<Rigidbody2D>();
        }

        public override void FixedUpdateNetwork()
        {
            FlipRobot();

            void FlipRobot()
            {
                var newScale = xfm.localScale;
                newScale.x = FacingDirection * Mathf.Abs(newScale.x);
                xfm.localScale = newScale;
            }
        }

        public void Move(ActionData data)
        {
            var movingLeft = data.Buttons.IsSet(ActionButton.MoveLeft);
            var movingRight = data.Buttons.IsSet(ActionButton.MoveRight);
            if (!movingLeft && !movingRight)
            {
                movementAnimator.SetBool(ShouldWalk, false);
                return;
            }

            if (rb2d.velocity.y > 0)
            {
                movementAnimator.SetBool(ShouldWalk, true);
            }

            // The facing direction must be updated based on the input
            if ((movingLeft && FacingDirection > 0) || (movingRight && FacingDirection < 0))
            {
                FacingDirection *= -1;
            }

            // This velocity is only applied while input is received, making the robot slow down as if it were skidding on ice otherwise
            // Can be altered using physics materials
            var newV = rb2d.velocity;
            newV.x = FacingDirection * moveMaxSpeed;
            rb2d.velocity = newV;
        }

        public void Jump(NetworkButtons pressedButtons)
        {
            if (!pressedButtons.IsSet(ActionButton.Jump)) return;

            // Only allow jumping when not already flying upwards
            if (rb2d.velocity.y <= 0)
            {
                rb2d.AddForce(new Vector2(0f, jumpImpulseY), ForceMode2D.Impulse);
            }
        }

        public void AimGun(ActionData data)
        {
            var eulerAngles = gunHandShoulder.eulerAngles;

            if (data.TreatAimAngleAsDelta)
            {
                eulerAngles.z = (float)Angle.Clamp(eulerAngles.z + data.AimAngle, 0f, maxAimAngle);
            }
            else
            {
                eulerAngles.z = (float)Angle.Clamp(data.AimAngle, 0f, maxAimAngle);
            }

            gunHandShoulder.eulerAngles = eulerAngles;
        }
    }
}