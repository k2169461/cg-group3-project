using System;
using System.Collections.Generic;
using System.Linq;
using Fusion;
using UnityEngine;
using Assert = UnityEngine.Assertions.Assert;

namespace CG.Group3.Project.Game
{
    [CreateAssetMenu(menuName = "Create LegalCombosScriptableObject", fileName = "LegalCombosScriptableObject",
        order = 0)]
    public class LegalCombosScriptableObject : ScriptableObject
    {
        private const string LegalComboKeys = "ABXY";
        // TODO correlate with tag definitions on federated controller subtypes 
        private const string LegalFcCodes = "mesrx_";

        [SerializeField]
        private SerializableDictionary<string, SerializableDictionary<string, ComboScriptableObject>> combos;

        public void OnValidate()
        {
            foreach (var fcTags in combos.Keys)
            {
                // Only two controllers participate
                Assert.AreEqual(2, fcTags.Length,
                    $"Expected only two Control Proxy tag characters. Found: {fcTags}");
            }

            // Check the controller tag pair keys against each other
            foreach (var (fcTags1, fcTags2) in
                     from tags1 in combos.Keys
                     from tags2 in combos.Keys
                     select (tags1, tags2))
            {
                if (fcTags1.Equals(fcTags2)) continue;

                // The existence of "ab" means "ba" must not exist
                Assert.IsTrue(!fcTags1.Equals(Reverse(fcTags2)),
                    $"Both {fcTags1} and {fcTags2} have been added. Please remove one.");

                static string Reverse(in string toReverse) =>
                    string.Create(toReverse.Length, toReverse, (chars, state) =>
                    {
                        state.AsSpan().CopyTo(chars);
                        chars.Reverse();
                    });
            }

            foreach (var (fcTags, combosForFCs) in combos)
            {
                var fcTagsSet = fcTags.ToHashSet();
                Assert.IsTrue(
                    // Exclude those tag sets which have wildcard '_' -- so validating individual tags becomes appropriate
                    fcTagsSet.IsProperSubsetOf(LegalFcCodes) && !fcTagsSet.Contains('_'),
                    $"Illegal controller codes: {string.Join(", ", fcTagsSet.Except(LegalFcCodes))}");

                foreach (var (sequence, comboScriptableObject) in combosForFCs)
                {
                    Assert.IsNotNull(comboScriptableObject,
                        $"No scriptable object assigned for combo sequence '{sequence}'");

                    // the sequence is expected to always have an even length since each combo step is encoded as
                    // one char for the button and a second char for the tag of the controller which pressed that button
                    Assert.IsTrue(
                        sequence.Length % 2 == 0,
                        $"Expected sequence composed of alternating key and controller codes, found inconsistent string length for sequence '{sequence}'.");

                    var keyCodes = new HashSet<char>();
                    var fcCodes = new HashSet<char>();

                    for (var i = 0; i < sequence.Length; i++)
                    {
                        var c = sequence[i];

                        var isEvenPos = i % 2 == 0;
                        // the button char is always uppercase and the controller tag char is always lowercase
                        Assert.IsTrue(
                            isEvenPos ? char.IsUpper(c) : char.IsLower(c),
                            $"Expected key code to be uppercase and controller code to be lowercase. Found '{c}' at index {i} in sequence '{sequence}'");

                        if (isEvenPos)
                        {
                            keyCodes.Add(c);
                            Assert.IsTrue(
                                keyCodes.IsSubsetOf(LegalComboKeys),
                                $"Illegal key code '{c}' at index {i} in sequence '{sequence}'");
                        }
                        else
                        {
                            if (fcCodes.Add(c))
                            {
                                Assert.IsTrue(
                                    fcCodes.Count <= 2,
                                    $"Found more controller codes than necessary in sequence '{sequence}'");
                            }
                            else
                            {
                                // Rely on short circuit behaviour
                                Assert.IsTrue(c == '_' || fcCodes.IsSubsetOf(fcTagsSet),
                                    $"Invalid controller code '{c}' at index {i} in sequence '{sequence}'");
                            }
                        }
                    }
                }
            }

            Debug.Log($"Validated {name} with no errors.");
        }
        
        public bool TryGetSequencesForFCsByTag(in char a, in char b,
            out SerializableDictionary<string, ComboScriptableObject> sequences)
        {
            Assert.IsTrue(a != b && LegalFcCodes.Contains(a) && LegalFcCodes.Contains(b));
            return combos.TryGetValue($"{a}{b}", out sequences) || combos.TryGetValue($"{b}{a}", out sequences);
        }
    }
}