﻿using Fusion;
using UnityEngine;

namespace CG.Group3.Project.Game
{
    public class RangedFederatedController : FederatedControllerBase
    {
        [SerializeField, Min(0.01f)] private float gamepadAimSpeedDegreesPerTick = 10f;
        [SerializeField, Min(0.01f)] private float mouseAimSpeedUnitsPerTick = 0.5f;
        private Camera mainCamera;

        public override char Tag => 'r';
        public override int Index => 4;

        private void Awake()
        {
            mainCamera = Camera.main;
        }

        public override bool ProduceInput(ref ActionData data)
        {
            var baseInput = base.ProduceInput(ref data);

            var stickAxisValue = Controls.Robot.AimGunStick.ReadValue<float>();
            var mouseScreenPosition = Controls.Robot.AimGunMousePosition.ReadValue<Vector2>();
            var mouseDelta = Controls.Robot.AimGunMouseDelta.ReadValue<Vector2>();

            // Prefer stick input if detected
            if (Mathf.Abs(stickAxisValue) > Mathf.Epsilon)
            {
                data.TreatAimAngleAsDelta = true;
                data.AimAngle = stickAxisValue * gamepadAimSpeedDegreesPerTick;
                return true;
            }

            // Make sure this is unset so changing between mouse and gamepad doesn't mess up aiming
            data.TreatAimAngleAsDelta = false;
            var robotPosition = Robot.transform.position;
            var mouseWorldPosition = mainCamera.ScreenToWorldPoint(mouseScreenPosition);
            var aimOffset = mouseWorldPosition.y - robotPosition.y;
            data.AimAngle = mouseAimSpeedUnitsPerTick * aimOffset;
            return true;
        }

        protected override void ConsumeInput(ActionData data, out NetworkButtons buttonsPressedThisFrame)
        {
            base.ConsumeInput(data, out buttonsPressedThisFrame);
            Robot.AimGun(data);
        }
    }
}