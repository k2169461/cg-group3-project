using System;
using System.Collections.Generic;
using System.Linq;
using CG.Group3.Project.Sandbox;
using Fusion;
using UnityEngine;

namespace CG.Group3.Project.Game
{
    public class ComboNetworkedState : NetworkBehaviour
    {
        private const short NullFcTag = 0;
        [SerializeField] private SerializableDictionary<ComboSmState, float> comboStateTransitionDelays = new();
        [SerializeField] private LegalCombosScriptableObject legalCombos;
        [Networked] private TickTimer ComboStateTimer { get; set; }
        [Networked, Capacity(2)] private NetworkArray<short> FCsParticipatingInComboByTag => default;
        [Networked] private ComboSmState ComboSmState { get; set; } = ComboSmState.Neutral;
        [Networked, UnityNonSerialized] public int CurrentComboStep { get; private set; }
        private Dictionary<string, ComboScriptableObject> permissibleCombos;

        public override void FixedUpdateNetwork()
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.Log("FCs in combo: " + string.Join(", ", FCsParticipatingInComboByTag.Select(i => (char)i)));
#endif
        }

        public bool IsInComboSmState(in ComboSmState state, in NetworkRunner runner) =>
            // To be in a state, its timer must not have expired and must be running
            // Quirky due to how TickTimer methods are defined
            ComboSmState == state &&
            // if the state is neutral, we check if the timer is expired or not running, else the opposite
            ComboStateTimer.ExpiredOrNotRunning(runner) == (ComboSmState == ComboSmState.Neutral);

        public bool HasCompletedComboSmState(in ComboSmState state, in NetworkRunner runner) =>
            // To be at the end of a state is for its timer to have expired (it must still be running)
            // NOTE: this is different from checking if the timer is expired or not running on all paths
            ComboSmState == ComboSmState.Neutral &&
            !ComboStateTimer.ExpiredOrNotRunning(runner) ||
            ComboSmState == state && ComboStateTimer.Expired(runner);

        public void TransitToComboSmState(in ComboSmState newComboSmState, in NetworkRunner runner)
        {
            Assert.Check(newComboSmState == ComboSmState.Neutral ||
                         newComboSmState != ComboSmState.Neutral && runner);

            Debug.Log($"Combo state {ComboSmState} -> {newComboSmState}");
            ComboSmState = newComboSmState;
            // Invalidate the timer if state set to neutral -- makes it so that the timer has expired or is running
            // in that case, runner can be null as it is not used
            ComboStateTimer = newComboSmState == ComboSmState.Neutral
                ? TickTimer.None
                : TickTimer.CreateFromSeconds(runner, comboStateTransitionDelays[newComboSmState]);
        }

        public void DebugLogComboSmStateStuff(in FederatedControllerBase fc)
        {
#if UNITY_EDITOR || DEVELOPMENT_BUILD
            Debug.LogError(
                $"Combo State on {fc.name} | Timer:{ComboStateTimer.RemainingTime(fc.Runner)} | State:{ComboSmState}");
#endif
        }

        public void IncrementCurrentComboStep() => CurrentComboStep++;

        public bool TryGetAllCombos(out SerializableDictionary<string, ComboScriptableObject> allCombos)
        {
            // Note that we check for combo sequences using the stored participants but compare for button presses later on using the calling federated controller
            var fc0 = (char)FCsParticipatingInComboByTag[0];
            var fc1 = (char)FCsParticipatingInComboByTag[1];
            return legalCombos.TryGetSequencesForFCsByTag(fc0, fc1, out allCombos);
        }

        public void NarrowDownPermissibleCombos(
            in IDictionary<string, ComboScriptableObject> allCombos,
            in Func<KeyValuePair<string, ComboScriptableObject>, bool> searchCriteria) =>
            // Whittle down possible suspects using a simple approach (no trie data structures)
            permissibleCombos =
                (permissibleCombos?.Where(searchCriteria) ?? allCombos.Where(searchCriteria))
                .ToDictionary(kv => kv.Key, kv => kv.Value);

        // For an undefined combos dictionary, the permissible count/num should be theoretically infinite
        public int NumPermissibleCombos => permissibleCombos?.Count ?? int.MaxValue;

        public KeyValuePair<string, ComboScriptableObject> FirstPermissibleCombo => permissibleCombos.First();

        public void ResetGlobalComboState()
        {
            CurrentComboStep = 0;
            permissibleCombos = null;

            for (var i = 0; i < FCsParticipatingInComboByTag.Length; i++)
            {
                FCsParticipatingInComboByTag.Set(i, NullFcTag);
            }
        }

        public void RegisterFc(in FederatedControllerBase fc)
        {
            Debug.Log($"Registering {fc.name} for combos...");
            var fcTag = (short)fc.Tag;

            // We assume only two controllers participate
            var fc0 = FCsParticipatingInComboByTag[0];
            var fc1 = FCsParticipatingInComboByTag[1];

            if (fc0 != NullFcTag && fc1 != NullFcTag) return;
            // Don't want to store the same controller twice
            if (fc0 == fcTag || fc1 == fcTag) return;

            FCsParticipatingInComboByTag.Set(fc0 == NullFcTag ? 0 : 1, fcTag);
        }

        public void UnregisterFc(in FederatedControllerBase fc)
        {
            Debug.Log($"Unregistering {fc.name} for combos...");
            var fcTag = (short)fc.Tag;

            if (FCsParticipatingInComboByTag[0] == fcTag)
            {
                FCsParticipatingInComboByTag.Set(0, NullFcTag);
            }
            else if (FCsParticipatingInComboByTag[1] == fcTag)
            {
                FCsParticipatingInComboByTag.Set(1, NullFcTag);
            }
        }

        public bool IsFcRegisteredForCombos(in FederatedControllerBase fc) =>
            FCsParticipatingInComboByTag.Contains((short)fc.Tag);

        public bool AreAllComboFCsRegistered => FCsParticipatingInComboByTag.All(fcTag => fcTag != NullFcTag);

        public bool AreAllComboFCsUnregistered => FCsParticipatingInComboByTag.All(fcTag => fcTag == NullFcTag);
    }
}