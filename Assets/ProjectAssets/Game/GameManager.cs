using System;
using System.Collections.Generic;
using System.Linq;
using CG.Group3.Project.UI;
using CG.Group3.Project.Utility;
using Fusion;
using Fusion.Sockets;
using UnityEngine;
using UnityEngine.SceneManagement;

namespace CG.Group3.Project.Game
{
    public class GameManager : MonoBehaviour, INetworkRunnerCallbacks
    {
        [SerializeField] private NetworkRunner networkRunnerPrefab;
        [SerializeField] private NetworkPrefabRef rpcForwarderPrefab;
        [SerializeField, Min(2)] private int numPlayers = 2;
        [SerializeField] private string gameSceneName = "LEVEL-1";
        [SerializeField] private string loginSceneName = "Login";
        private GraphicButtonInteractionProvider gbip;
        private NetworkRunner networkRunner;
        private ActionData actionData;

        private readonly List<(PlayerRef player, NetworkPrefabRef prefab, FederatedControllerBase fc)>
            db = new();
        private NetworkObject rpcForwarder;

        // For use with Unity Events
        [EnumAction(typeof(GameMode))]
        public void StartGame(int gameMode) => StartGame((GameMode)gameMode);

        public async void StartGame(GameMode mode)
        {
            DontDestroyOnLoad(gameObject);

            networkRunner = Instantiate(networkRunnerPrefab);
            if (!networkRunner)
            {
                throw new InvalidProgramException("Runner must be spawned properly. Invalid state reached.");
            }

            networkRunner.ProvideInput = true;
            networkRunner.AddCallbacks(this);

            // Start or join (depends on game mode) a session with a specific name
            await networkRunner.StartGame(new StartGameArgs
            {
                GameMode = mode,
                SessionName = "GameRoom",
                PlayerCount = numPlayers,
                SceneManager = gameObject.AddComponent<NetworkSceneManagerDefault>()
            });

            networkRunner.SetActiveScene(gameSceneName);
        }

        void INetworkRunnerCallbacks.OnPlayerJoined(NetworkRunner runner, PlayerRef player)
        {
        }

        //  Instructions for player selecting controller
        // - When the player clicks the respective button, all clients are notified that the same button should be disabled for all of them
        // - Host spawns the respective controller
        // - Clients are not aware of controllers on the same tick, except the host
        // - Host disables selection UI once all clients have chosen a controller

        public void AssignFcToPlayerImpl(NetworkPrefabRef fcPrefabRef, in PlayerRef inputAuthority)
        {
            var dbRow = gbip!.Db.FirstOrDefault(dbRow => dbRow.fcPrefabRef == fcPrefabRef);
            Assert.Check(
                dbRow != null, "Could not assign federated controller to player due to invalid prefab ref mapping");

            // Guard against spawning twice
            if (db.Any(row => row.prefab == fcPrefabRef)) return;

            var fcNetworkObject = networkRunner.Spawn(fcPrefabRef, Vector3.zero, Quaternion.identity, inputAuthority);
            gbip!.RpcDimOutGraphicButton(fcPrefabRef);

            // This is null for clients.
            if (!fcNetworkObject) return;

            db.Add((player: inputAuthority, fcPrefabRef, fc: fcNetworkObject.GetComponent<FederatedControllerBase>()));

            // Disable the UI if all players have chosen their controllers
            if (db.Count == numPlayers)
            {
                gbip!.RpcActivateSelf(false);
            }
        }

        void INetworkRunnerCallbacks.OnPlayerLeft(NetworkRunner runner, PlayerRef player)
        {
            if (!runner.IsPlayerValid(player)) return;

            // Return the controller to the set of available controllers and remove the player
            var dbRow = db.FirstOrDefault(dbRow => dbRow.player == player);
            if (dbRow == default) return;

            runner.Despawn(dbRow!.fc.Object);
            db.Remove(dbRow!);
        }

        private void Update()
        {
            // Care should be taken that the controllers don't overwrite each other each time
            foreach (var fc in db.Select(row => row.fc).Where(fc => fc && fc.HasInputAuthority))
            {
                if (fc.ProduceInput(ref actionData))
                {
                    Debug.Log($"Input produced by {fc.Object.InputAuthority} from type {fc.GetType()}");
                }
            }
        }

        void INetworkRunnerCallbacks.OnInput(NetworkRunner runner, NetworkInput input)
        {
            input.Set(actionData);
            actionData = default;
        }

        void INetworkRunnerCallbacks.OnInputMissing(NetworkRunner runner, PlayerRef player, NetworkInput input)
        {
        }

        void INetworkRunnerCallbacks.OnShutdown(NetworkRunner runner, ShutdownReason shutdownReason)
        {
            SceneManager.LoadScene(loginSceneName);
        }

        // ReSharper disable once Unity.IncorrectMethodSignature
        void INetworkRunnerCallbacks.OnConnectedToServer(NetworkRunner runner)
        {
        }

        void INetworkRunnerCallbacks.OnDisconnectedFromServer(NetworkRunner runner)
        {
        }

        void INetworkRunnerCallbacks.OnConnectRequest(NetworkRunner runner,
            NetworkRunnerCallbackArgs.ConnectRequest request, byte[] token)
        {
        }

        void INetworkRunnerCallbacks.OnConnectFailed(NetworkRunner runner, NetAddress remoteAddress,
            NetConnectFailedReason reason)
        {
        }

        void INetworkRunnerCallbacks.OnUserSimulationMessage(NetworkRunner runner, SimulationMessagePtr message)
        {
        }

        void INetworkRunnerCallbacks.OnSessionListUpdated(NetworkRunner runner, List<SessionInfo> sessionList)
        {
        }

        void INetworkRunnerCallbacks.OnCustomAuthenticationResponse(NetworkRunner runner,
            Dictionary<string, object> data)
        {
        }

        void INetworkRunnerCallbacks.OnHostMigration(NetworkRunner runner, HostMigrationToken hostMigrationToken)
        {
        }

        void INetworkRunnerCallbacks.OnReliableDataReceived(NetworkRunner runner, PlayerRef player,
            ArraySegment<byte> data)
        {
        }

        void INetworkRunnerCallbacks.OnSceneLoadDone(NetworkRunner runner)
        {
            if (rpcForwarder) return;
            rpcForwarder = networkRunner.Spawn(rpcForwarderPrefab, null, null, null, (_, no) =>
            {
                if (!gbip)
                {
                    gbip = FindObjectOfType<GraphicButtonInteractionProvider>();
                }

                if (!gbip)
                {
                    throw new ApplicationException(
                        $"This function cannot be called in a scene without a {nameof(GraphicButtonInteractionProvider)}.");
                }

                no.GetComponent<GameManagerRpcForwarder>().Init(this, gbip);
            });
        }

        void INetworkRunnerCallbacks.OnSceneLoadStart(NetworkRunner runner)
        {
        }
    }
}