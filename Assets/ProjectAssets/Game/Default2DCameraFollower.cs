using UnityEngine;

namespace CG.Group3.Project.Game
{
    public class Default2DCameraFollower : MonoBehaviour
    {
        [SerializeField] private Transform target;
        [SerializeField] private Vector3 offset = new(0f, 0f, -10f);
        [SerializeField] private float smoothTime = 0.25f;
        private Vector3 velocity = Vector3.zero;

        private void Update()
        {
            transform.position =
                Vector3.SmoothDamp(transform.position, target.position + offset, ref velocity, smoothTime);
        }
    }
}