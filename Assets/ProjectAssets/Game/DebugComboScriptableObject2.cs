using CG.Group3.Project.UI;
using UnityEngine;

namespace CG.Group3.Project.Game
{
    [CreateAssetMenu(menuName = "Create DebugComboScriptableObject2", fileName = "DebugComboScriptableObject2", order = 0)]
    public class DebugComboScriptableObject2 : ComboScriptableObject
    {
        private DebugCanvas debugCanvas;
        
        public override void DoAction(FederatedControllerBase callingFederatedController)
        {
            if (!debugCanvas)
            {
                debugCanvas = FindObjectOfType<DebugCanvas>();
            }
            
            debugCanvas!.RpcPrintTextToScreen("Performed debug combo!");
        }
    }
}
