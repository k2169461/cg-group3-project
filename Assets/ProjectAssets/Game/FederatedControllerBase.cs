using System;
using CG.Group3.Project.Sandbox;
using Fusion;
using UnityEngine;

namespace CG.Group3.Project.Game
{
    public abstract class FederatedControllerBase : NetworkBehaviour
    {
        private enum ComboSequenceStage
        {
            Failed,
            Running,
            Complete
        }

        [Networked] private NetworkButtons PreviousButtonsState { get; set; }
        private GameManager gameManager;
        protected Robot Robot;
        protected static Controls Controls;
        private ComboNetworkedState comboNetworkedState;

        /// Used for identifying this federated controller in the context of combo sequences
        public virtual char Tag => '_';

        /// Used for identifying this federated controller in the context of internal button mangling representation
        public virtual int Index => 0;

        private void Awake() => Controls = new Controls();

        private void OnEnable() => Controls.Robot.Enable();

        private void OnDisable() => Controls.Robot.Disable();

        public override void Spawned()
        {
            if (!comboNetworkedState)
            {
                comboNetworkedState = FindObjectOfType<ComboNetworkedState>();
            }
        }

        public virtual bool ProduceInput(ref ActionData data)
        {
            var comboStartPressed = Controls.Robot.ComboStart.WasPressedThisFrame();
            var comboEngageAPressed = Controls.Robot.ComboEngageA.WasPressedThisFrame();
            var comboEngageBPressed = Controls.Robot.ComboEngageB.WasPressedThisFrame();
            var comboEngageXPressed = Controls.Robot.ComboEngageX.WasPressedThisFrame();
            var comboEngageYPressed = Controls.Robot.ComboEngageY.WasPressedThisFrame();

            // ReSharper disable once ConvertIfStatementToSwitchStatement
            if (!comboStartPressed &&
                !comboEngageAPressed && !comboEngageBPressed && !comboEngageXPressed && !comboEngageYPressed)
                return false;

            // Only one of these buttons can be pressed in a frame.
            var msg = $"Combo button pressed by {this}: ";

            do
            {
                if (comboStartPressed)
                {
                    data.Buttons.SetDown(ActionButton.ComboStart);
                    msg += "Start";
                    break;
                }

                if (comboEngageAPressed)
                {
                    data.Buttons.SetDown(ActionButton.ComboEngageA.ForFc(this));
                    msg += "A";
                    break;
                }

                if (comboEngageBPressed)
                {
                    data.Buttons.SetDown(ActionButton.ComboEngageB.ForFc(this));
                    msg += "B";
                    break;
                }

                if (comboEngageXPressed)
                {
                    data.Buttons.SetDown(ActionButton.ComboEngageX.ForFc(this));
                    msg += "X";
                    break;
                }

                data.Buttons.SetDown(ActionButton.ComboEngageY.ForFc(this));
                msg += "Y";
                break;
#pragma warning disable CS0162
                // Hack to get neat breaks in code
            } while (false);
#pragma warning restore CS0162

            Debug.Log(msg);
            return true;
        }

        public override void FixedUpdateNetwork()
        {
            if (!GetInput(out ActionData data)) return;
            Debug.Log($"InputAuthority {Object.InputAuthority}, Data {Convert.ToString(data.Buttons.Bits, 2),4}");
            ConsumeInput(data, out var buttonsPressedThisFrame);

            // Technically input consumption but perhaps better placed directly in FUN
            RunComboStateMachine(this, buttonsPressedThisFrame);

            comboNetworkedState.DebugLogComboSmStateStuff(this);

            static void RunComboStateMachine(in FederatedControllerBase self, NetworkButtons buttonsPressed)
            {
                var comboStartButtonPressed = buttonsPressed.IsSet(ActionButton.ComboStart);

                // The first controller sets the state
                if (Is(ComboSmState.Neutral, self) && comboStartButtonPressed &&
                    !self.comboNetworkedState.IsFcRegisteredForCombos(self))
                {
                    self.comboNetworkedState.RegisterFc(self);
                    To(ComboSmState.Initializing, self);
                    return;
                }

                // The second controller joins later
                if (Is(ComboSmState.Initializing, self) && comboStartButtonPressed &&
                    !self.comboNetworkedState.IsFcRegisteredForCombos(self))
                {
                    self.comboNetworkedState.RegisterFc(self);
                    return;
                }

                // Reverse combo initialization if controllers opt out after initialization phase has started
                if (Is(ComboSmState.Initializing, self) && comboStartButtonPressed &&
                    self.comboNetworkedState.IsFcRegisteredForCombos(self))
                {
                    self.comboNetworkedState.UnregisterFc(self);

                    if (!self.comboNetworkedState.AreAllComboFCsUnregistered) return;

                    To(ComboSmState.Neutral, self);
                    self.comboNetworkedState.ResetGlobalComboState();

                    return;
                }

                if (Done(ComboSmState.Initializing, self))
                {
                    if (self.comboNetworkedState.AreAllComboFCsRegistered)
                    {
                        To(ComboSmState.Engaged, self);
                    }
                    else
                    {
                        To(ComboSmState.Neutral, self);
                        self.comboNetworkedState.ResetGlobalComboState();
                    }

                    return;
                }

                if (Is(ComboSmState.Engaged, self))
                {
                    switch (RunSequence(buttonsPressed, self))
                    {
                        case ComboSequenceStage.Complete:
                            // Only cool down after successful runs
                            To(ComboSmState.CoolingDown, self);
                            self.comboNetworkedState.ResetGlobalComboState();
                            return;

                        case ComboSequenceStage.Failed:
                            // We don't penalise the player by making them wait on failure; instead, we shortcut them back to neutral state
                            To(ComboSmState.Neutral, self);
                            self.comboNetworkedState.ResetGlobalComboState();
                            return;

                        case ComboSequenceStage.Running:
                            return;

                        default:
                            // Unity does not support UnreachableException
                            throw new InvalidOperationException("Unreachable Code!");
                    }
                }

                // The Engaged timer ran out as the combo sequence wasn't completed
                if (Done(ComboSmState.Engaged, self))
                {
                    To(ComboSmState.Neutral, self);
                    self.comboNetworkedState.ResetGlobalComboState();
                    return;
                }

                // If cooldown is not complete, skip execution
                if (Is(ComboSmState.CoolingDown, self)) return;

                if (Done(ComboSmState.CoolingDown, self))
                {
                    To(ComboSmState.Neutral, self);
                }

                #region Definitions

                // Shorter alias for method call
                static bool Is(in ComboSmState state, in FederatedControllerBase self) =>
                    self.comboNetworkedState.IsInComboSmState(state, self.Runner);

                // Shorter alias for method call
                static bool Done(in ComboSmState state, in FederatedControllerBase self) =>
                    self.comboNetworkedState.HasCompletedComboSmState(state, self.Runner);

                // Shorter alias for method call
                static void To(in ComboSmState newState, in FederatedControllerBase self) =>
                    self.comboNetworkedState.TransitToComboSmState(newState, self.Runner);

                static ComboSequenceStage RunSequence(
                    NetworkButtons buttonsPressedThisFrame,
                    FederatedControllerBase self)
                {
                    var comboEngageASet =
                        buttonsPressedThisFrame.IsSet(ActionButton.ComboEngageA.ForFc(self));
                    var comboEngageBSet =
                        buttonsPressedThisFrame.IsSet(ActionButton.ComboEngageB.ForFc(self));
                    var comboEngageXSet =
                        buttonsPressedThisFrame.IsSet(ActionButton.ComboEngageX.ForFc(self));
                    var comboEngageYSet =
                        buttonsPressedThisFrame.IsSet(ActionButton.ComboEngageY.ForFc(self));

                    const char wrongButton = ' ';
                    // Track one key press at a time
                    var keyCode =
                        comboEngageASet ? 'A' :
                        comboEngageBSet ? 'B' :
                        comboEngageXSet ? 'X' :
                        comboEngageYSet ? 'Y' : wrongButton;

                    // Don't penalise players for clicking the wrong button
                    if (keyCode == wrongButton)
                    {
                        Debug.Log("[COMBO RUNNING] The wrong button or no button was pressed.");
                        return ComboSequenceStage.Running;
                    }

                    Debug.Log($"Registered {keyCode} from {self.name}");

                    // Dealing with even-length sequences which always look like AaBb
                    // where uppercase letters can be A, B, X, Y and lowercase letters are the federated controller's tags.
                    var keyCodePos = 2 * self.comboNetworkedState.CurrentComboStep;
                    var fcCodePos = keyCodePos + 1;

                    var prevCount = self.comboNetworkedState.NumPermissibleCombos;

                    if (!self.comboNetworkedState.TryGetAllCombos(out var allCombos))
                    {
                        Debug.Log("[COMBO FAILED] Combo sequences could not be retrieved.");
                        return ComboSequenceStage.Failed;
                    }

                    self.comboNetworkedState.NarrowDownPermissibleCombos(allCombos, pair =>
                    {
                        var sequence = pair.Key;
                        // '_' means that specific key has been pressed by any of the participating controllers
                        return sequence[keyCodePos] == keyCode &&
                               (sequence[fcCodePos] == self.Tag || sequence[fcCodePos] == '_');
                    });
                    var currCount = self.comboNetworkedState.NumPermissibleCombos;

                    Debug.Log(
                        $"{nameof(prevCount)}:{prevCount}, {nameof(currCount)}:{currCount}, {nameof(self.comboNetworkedState.CurrentComboStep)}:{self.comboNetworkedState.CurrentComboStep}");

                    // Make sure we're making progress towards completing the sequence
                    if (currCount == 0)
                    {
                        Debug.Log("[COMBO FAILED] No combo sequences match the input.");
                        return ComboSequenceStage.Failed;
                    }

                    if (currCount <= prevCount)
                    {
                        self.comboNetworkedState.IncrementCurrentComboStep();
                    }

                    if (currCount == 1)
                    {
                        // We know there is one and exactly one permissible combo object
                        var (sequence, comboScriptableObject) = self.comboNetworkedState.FirstPermissibleCombo!;

                        if (fcCodePos == sequence.Length - 1)
                        {
                            // This object is validated to never be null
                            comboScriptableObject!.DoAction(self);

                            Debug.Log("[COMBO COMPLETE]");
                            return ComboSequenceStage.Complete;
                        }
                    }

                    Debug.Log(
                        $"[COMBO RUNNING] Waiting for inputs. Current combo step: {self.comboNetworkedState.CurrentComboStep}");
                    return ComboSequenceStage.Running;
                }

                #endregion
            }
        }

        protected virtual void ConsumeInput(ActionData data, out NetworkButtons buttonsPressedThisFrame)
        {
            if (!gameManager)
            {
                gameManager = FindObjectOfType<GameManager>();
            }

            if (!Robot)
            {
                Robot = FindObjectOfType<Robot>();
            }

            if (!gameManager || !Robot)
            {
                Debug.LogError($"{nameof(gameManager)} = <{gameManager}>,{nameof(Robot)} = <{Robot}>");
                buttonsPressedThisFrame = default;
                return;
            }

            buttonsPressedThisFrame = data.Buttons.GetPressed(PreviousButtonsState);
            // Cache button presses between frames as suggested in Photon Fusion documentation
            PreviousButtonsState = data.Buttons;
        }
    }
}