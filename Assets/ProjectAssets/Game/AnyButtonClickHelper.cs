using CG.Group3.Project.Sandbox;
using Fusion;
using UnityEngine;
using UnityEngine.InputSystem;

namespace CG.Group3.Project.Game
{
    public class AnyButtonClickHelper : MonoBehaviour
    {
        private Controls controls;

        private void Awake() => controls = new Controls();

        private void OnEnable()
        {
            controls.LoginMenu.Enable();
            controls.LoginMenu.Any.performed += AnyKeyPressed;
        }

        private void OnDisable()
        {
            controls.LoginMenu.Any.performed -= AnyKeyPressed;
            controls.LoginMenu.Disable();
        }

        private static void AnyKeyPressed(InputAction.CallbackContext context) =>
            FindObjectOfType<GameManager>().StartGame(GameMode.AutoHostOrClient);
    }
}
