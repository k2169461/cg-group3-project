using UnityEngine;

namespace CG.Group3.Project.Game
{
    public abstract class ComboScriptableObject : ScriptableObject
    {
        public abstract void DoAction(FederatedControllerBase callingFederatedController);
    }
}
